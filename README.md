# iCareBase's recorded history dates
------------

## Event Lạng Sơn 21/09/2019
* Photo: https://photos.app.goo.gl/9dw3hZKPf4e2jDvk7

------------
## Event Đà Nẵng 21/09/2019
* Photo: https://photos.app.goo.gl/7D5Jhxba8RJDPEV97

------------
## Event Vĩnh Phúc 15/09/2019
* Photo: https://photos.app.goo.gl/71RQWyLsxskuafYX7

------------
## Event 08-09-2019
### Dành cho giới chuyên môn (Bác Sĩ)
* Photo: https://photos.app.goo.gl/yJBvTcbCRV4MtrwG7
* Video: https://www.youtube.com/watch?v=JxdGdF_U6Vk
* VTV1 đưa tin: https://www.youtube.com/watch?v=2Y0kTsxNehU
* VnExpress đưa tin: https://vnexpress.net/kinh-doanh/ifg-dua-cong-nghe-sang-loc-ung-thu-ve-viet-nam-3980792.html

### Event trưa:
* Photo: https://photos.app.goo.gl/UhKRRnzTmBVDsXJy6
* Video: https://www.youtube.com/watch?v=X_vPEqHkxwY

------------
## Event 24-08-2019
### Đà Nẵng:
* Photo: https://photos.app.goo.gl/K7wwJfS2DorwFjaTA
* Video: https://www.youtube.com/watch?v=u2sII73vHVo

### TP.HCM:
* Photo: https://photos.app.goo.gl/ixE4yjPLMK4kQsCRA
* Video: https://www.youtube.com/watch?v=rvJYeUocYO0

------------
## Event Tây Ninh | 11-08-2019
* Photo: https://photos.app.goo.gl/mpuLKaaAQYTe26pY7

------------
## Event iCareBase Holiday 26-28/07/2019
* Photo: https://photos.app.goo.gl/wbFkUAVopxh9QeZD7

------------
## Event TP.HCM - 21/07/2019
* Photo: https://photos.app.goo.gl/pEEaC4aRtWVpmVDP9

------------
## Event Gia Lai - 21/07/2019
* Photo: https://photos.app.goo.gl/uFzckRQbNiaP2MEd6
* Video: https://www.youtube.com/watch?v=IXIFKr48Lf4

------------
## Event Đà Nẵng 13/07/2019
* Photo: https://photos.app.goo.gl/wTpFwCMEFrqwJLHGA
* Video: https://www.youtube.com/watch?v=Ln7LFyV_iwE

------------
## Event TP.HCM 10/07/2019
* Photo: https://photos.app.goo.gl/VyecVuZgE69a7SWs8

------------
## Event Thái Nguyên 04/07/2019
* Photo: https://photos.app.goo.gl/NiWxxvu9nRzsszvx6
* Video: https://youtu.be/_vIiCNkxF1I

------------
## Event TP.HCM 25/06/2019
* Photo: https://photos.app.goo.gl/a6eKBkRhYWNJcJQH9
* Video: https://www.youtube.com/watch?v=8xoC7FU7dWg

------------
## Event Hà Nội 22/06/2019
* Video: https://www.youtube.com/watch?v=RmN8rDy1lqU

------------
## Event Hà Nội 21/06/2019
* Photo: https://photos.app.goo.gl/xKN1SzhzCtqPDL4YA
* Video: https://youtu.be/NfNP526uDJA

------------
## Event TP.HCM 19/06/2019: Đêm nhạc Vì Yêu
* Video: https://www.youtube.com/watch?v=ucWpbNYJkZE

------------
## Event iCareBase Holiday: 13-15/06/2019
* Photo: https://photos.app.goo.gl/e7uWngUVKz1MDWFB8
* Video: https://www.youtube.com/watch?v=ivpvsij1YsM
* Bản tin Truyền hình: https://www.youtube.com/watch?v=EguRpIHFZaE

------------
## EVENT HÀ NỘI: 07/06/2019
* Photo: 

------------
## Event TP.HCM 27/05/2019
* Video: https://www.youtube.com/watch?v=ROa68qCr_8E

------------
## Event Đà Nẵng 22/05/2019:
* Video: https://www.youtube.com/watch?v=dCbE8tpGhp0

------------
## Event 12/05/2019: Ra mắt dự án iCareBase tại Hải Dương
* Photo: https://photos.app.goo.gl/ykh9k5DCKn8PY98t7
* Video: https://www.youtube.com/watch?v=r_FtxUjtWdg

------------
## Event 10/05/2019: Ra mắt partner tại Đà Nẵng và giới thiệu chương trình phòng chống ung thư từ gốc
* Photo: https://photos.app.goo.gl/UB3t2dAszT4A2ArM7
* Video: https://www.youtube.com/watch?v=cevnr-Ei66Y

------------
## Event 09/05/2019: 09/05/2019: Ra mắt các partner/ Đại sứ tại HCM
* Photo: https://photos.app.goo.gl/k8w7bC1TTavqmcQm7
* Video: https://www.youtube.com/watch?v=edrop6t992w

------------
## Event 05/05/2019: Ra mắt đại sứ Phía Bắc
* Photo: https://photos.app.goo.gl/L4xucqw36xkHPVwk8

------------
## Chuỗi Event tháng 04/2019
* Photo: https://photos.app.goo.gl/HwESZWF3feKLiNT17

------------
## Event 16/03/2019:
* Photo: https://photos.app.goo.gl/KfksyRMmRNp1x9Py9
* Video: https://www.youtube.com/watch?v=vevhVTRYH-Q
------------